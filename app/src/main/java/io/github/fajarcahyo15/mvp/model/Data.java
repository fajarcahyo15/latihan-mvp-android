package io.github.fajarcahyo15.mvp.model;

/**
 * Created by root on 4/5/18.
 */

public class Data {
    private String text;

    public Data() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
