package io.github.fajarcahyo15.mvp.presenter;

import io.github.fajarcahyo15.mvp.model.Data;

/**
 * Created by root on 4/5/18.
 */

public class MainPresenter implements Presenter<MainView> {

    private MainView mView;

    public void onAttach(final MainView view){
        mView = view;
    }

    public void onDetach() {
        mView = null;
    }

    public void showFragment() {
        final Data data = new Data();
        data.setText("Hello From Data!");

        mView.onShowFragment(data);
    }

}
