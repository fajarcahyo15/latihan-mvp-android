package io.github.fajarcahyo15.mvp.presenter;

import io.github.fajarcahyo15.mvp.model.Data;

/**
 * Created by root on 4/5/18.
 */

public interface Presenter<T extends View> {
    void onAttach(T view);

    void onDetach();
}
