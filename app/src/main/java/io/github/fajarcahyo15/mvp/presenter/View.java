package io.github.fajarcahyo15.mvp.presenter;

/**
 * Created by root on 4/5/18.
 */

public interface View {
    void onAttachView();

    void onDetachView();
}
